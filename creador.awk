$1 == "HETATM" {
	atomo = $3
	residuo = $4
	cadena = $5
	orden = $6

	cor_x = $7
	cor_y = $8
	cor_z = $9
	indice = cadena "_" residuo "_" orden
	#se van guardando la  sumatoria de los datos en un arreglo único para cada ion o ligando
	coordenadas[indice]["total"] = 1 + coordenadas[indice]["total"]
	coordenadas[indice]["x"] = cor_x + coordenadas[indice]["x"]
	coordenadas[indice]["y"] = cor_y + coordenadas[indice]["y"]
	coordenadas[indice]["z"] = cor_z + coordenadas[indice]["z"]
}

$1 == "ATOM" {
	id_atomo = $2
	atomo = $3
	aminoacido = $4
	cadena = $5
	id_aminoacido = $6


	cor_x = $7
	cor_y = $8
	cor_z = $9

	indice_aa = cadena "_" aminoacido "_" id_aminoacido
	indice_atomo = id_atomo "_" atomo
	aminoacidos[indice_aa] = "no"
	atomos[indice_aa][indice_atomo]["x"] = cor_x
	atomos[indice_aa][indice_atomo]["y"] = cor_y
	atomos[indice_aa][indice_atomo]["z"] = cor_z
}

END {
	for (indice in coordenadas) {
		total = coordenadas[indice]["total"]

		if (total != 1) {
			coordenadas[indice]["x"] = coordenadas[indice]["x"] / total
			coordenadas[indice]["y"] = coordenadas[indice]["y"] / total
			coordenadas[indice]["z"] = coordenadas[indice]["z"] / total
		}

		#print "centro geometrico de " indice " es:"
		#print coordenadas[indice]["x"], coordenadas[indice]["y"], coordenadas[indice]["z"]
	}

	# ver si se tiene que dibujar
	for (ligando in coordenadas) {
		cg_x = coordenadas[ligando]["x"]
		cg_y = coordenadas[ligando]["y"]
		cg_z = coordenadas[ligando]["z"]

		for (aminoacido_indice in aminoacidos) {
			for (indice in atomos[aminoacido_indice]) {
				atomo_x = atomos[aminoacido_indice][indice]["x"]
				atomo_y = atomos[aminoacido_indice][indice]["y"]
				atomo_z = atomos[aminoacido_indice][indice]["z"]
				dist = ((atomo_x - cg_x)^2 + (atomo_y - cg_y)^2 + (atomo_z - cg_z)^2)^0.5

				if (dist <= distancia) {
					dibujar[ligando][aminoacido_indice] = "si"
					break 
					#dejar de procesar atomos de este aminoacido
				}
			}
		}
	}

	for (ligando in dibujar) {
		archivo = "ligando_" ligando "_" distancia ".dot"

		print "digraph G {" > archivo
		print "rankdir = LR;" >> archivo
		print "" >> archivo
		print "" >> archivo
		print ligando >> archivo
		print "" >> archivo

		print "# aminoacidos" >> archivo
		for (aminoacido in dibujar[ligando]) {
			split(aminoacido, aa, "_")
			split (ligando, li, "_")
			print aminoacido " [label = \""aa[2]"\"]" >> archivo
			print ligando " [label = \""li[2]"\"]" >> archivo
			print aminoacido " -> " ligando >> archivo
			#print "dibujar " aminoacido " alrededor de " ligando >> archivo
		}

		print "# atomos" >> archivo
		for (aminoacido in dibujar[ligando]) {
			for (indice_atomo in atomos[aminoacido]) {
				split(indice_atomo, at,"_")

				print aminoacido "_" at[2] " [label = \""at[2]"\"]" >> archivo
				print aminoacido "_" at[2] " -> " aminoacido >> archivo


			}
		}

		print "}" >> archivo
	}
}

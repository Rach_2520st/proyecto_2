Visualiza Ligandos según distancia predeterminada:

Este programa se caracteriza por la visualización de cada ligando o ión en una proteína específica (esta se debe encontrar en la base de datos de pdb), el usuario ingresa la distancia en la que quiere visualizar lo que rodea a este ligando, gracias a esto se establece un radio según la distancia ingresada por el usuario y se puede observar que aminoácidos se encuentran en ese radio (si al menos un elemento del aminoácido se encuentra dentro de ese radio, se debe visualizar el aminoácido completo)

Obtención del programa:

Para la obtención de este programa el primer paso a seguir es clonar el presente repositorio y luego abrir una terminal, después de eso escribir "bash protein.sh", luego de esto ingresar la distancia en la que quiere visualizar lo que rodea al ligando, para observar las imagenes generadas ingresa a la carpeta contenedora del programa y cada ligando estará con su respectivo nombre.

Prerrequisitos:

Encontrarse en un entorno Linux
Tener instalada la librería graphviz

Objetivos:

Permitir visualizar todos los ligandos de una proteína y lo que los rodea hasta cierta distancia ingresada por el usuario

Sobre el código:

El código se encarga de verificar si es que la cadena de texto ingresada por el usuario pertenece a un código que se encuentra en la base de datos, luego de esto verifica si la cadena de texto ingresad es una proteína, si lo es, se descarga y si no, aparece un mensaje de error.
Luego de esto el código filtra desde el pdb las lineas que contienen HETATOM, todos menos los que contienen HOH, luego de esto filtra los que comienzan con ATOM y  los lleva a un archivo llamado proteina.txt.
Después de esto se le pide al usuario ingresar la distancia a la que quiere ver los elementos y aminoácidos que rodean los ligandos o iones, el programa se encarga de verificar si lo ingresado es un número, si no lo es, envia un mensaje de error, si lo es le pasa el parámetro distancia a AWK, en AWK se calculan los centros geometricos de los ligandos, en caso de ser iones las coordenadas se mantienen tal y como son, luego de esto cálcula la distancia entre cada elemento y los ligandos, si esta distancia es menor o igual a la distancia ingresada por el usuario es entregada a los archivos.dot, para cada ligando se crea unarchivo dot para la generación de gráficas de ligandos, después de esto se genera la imagen en formato svg.


Autor: 

Rachell Scarlett Aravena Martínez.
#!/bin/bash

echo "ingresa el código de la proteína que quiere visualizar:"
read codigo

codigo=${codigo^^} #cambiar a mayusculas

if ! [[ ${#codigo} == 4 || "$codigo" =~ [A-Z0-9] ]]; then
	echo "ingresa un código valido, este debe contener el codigo correspondiente a la proteina que quieres visualizar"
	exit 1 #cierra la aplicación 
fi
#revisa si el código que se está entregando es una proteina
if ! grep "\"$codigo\"" bd-pdb.txt | grep '"Protein"'; then
	echo "El código ingresado no es una proteína o no se encuentra en la base de datos, intentelo nuevamente"
	exit 1 
fi

if  [[ ! -f $codigo.pdb ]]; then
	wget https://files.rcsb.org/download/$codigo.pdb
fi
#filtra las filas que comienzan con ATOM y con HETATM
#en el mismo archivo para evitar mas archivos 
grep ^HETATM $codigo.pdb |egrep -v "HOH" > info_$codigo.txt
grep ^ATOM $codigo.pdb | awk '{ print substr($0, 0, 16) " " substr($0, 18, 61) }' >> info_$codigo.txt 

echo "ingresa la distancia en Angstrom a la que quieres visualizar tus ligandos e iones"
read distancia

if ! [[ "$distancia" =~ ^[0-9]+$ ]]; then
	echo "la distancia debe ser un numero"
	exit 1
fi

#se va a hacer cada .dot
awk -f creador.awk -v distancia=$distancia info_$codigo.txt


#recorre los archivos que terminan en .dot y luego genera la imagen a partir de estos
for i in *.dot; do
	if [[ ! -f $i.svg ]]; then
		dot -Tsvg -o $i.svg $i
	fi
done

echo "para visualizar los ligandos de la proteína que descargaste ingresa al repositorio contenedor del programa y visualiza ligandos.svg"